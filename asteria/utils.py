def clamp(x, lower=0, upper=1):
    return min(max(x, lower), upper)
